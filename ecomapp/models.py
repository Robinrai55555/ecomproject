from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class EcommerceAdmin(models.Model):
    User = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=300)
    mobile_no = models.CharField(max_length=30, null=True, blank=True)

    def __str__(self):
        return self.user.username


class Customer(models.Model):
    User = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=300)
    image = models.ImageField(upload_to='customer')
    mobile_no = models.CharField(max_length=30, null=True, blank=True)
    email = models.EmailField()
    address = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.name


class ProductCategory(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(null=True, blank=True, unique=True)
    root = models.ForeignKey(
        'self', on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.title


class Product(models.Model):
    title = models.CharField(max_length=200)
    category = models.ForeignKey(ProductCategory, on_delete=models.CASCADE)
    product_id = models.CharField(max_length=100, unique=True)
    image = models.ImageField(upload_to='products')
    max_return_price = models.DecimalField(max_digits=10, decimal_places=3)
    sellingprice = models.DecimalField(max_digits=10, decimal_places=3)
    descriptions = models.TextField()
    #stock = models.PositiveIntegerField()

    def __str__(self):
        return self.title


class Cart(models.Model):
    Customer = models.ForeignKey(
        Customer, on_delete=models.SET_NULL, null=True, blank=True)
    total = models.DecimalField(max_digits=10, decimal_places=3)

    def __str__(self):
        return self.customer.name


class CartProduct(models.Model):
    Cart = models.ForeignKey(ProductCategory, on_delete=models.CASCADE)
    Product = models.OneToOneField(Product, on_delete=models.CASCADE)
    qunatity = models.PositiveIntegerField(default=1)
    rate = models.DecimalField(max_digits=10, decimal_places=3)
    subtotal = models.DecimalField(max_digits=15, decimal_places=3)

    def __str__(self):
        return self.product.title
    
